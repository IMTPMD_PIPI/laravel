<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
          'id' => $this->id,
          'name' => $this->name,
          'price' => $this->price,
          'unit' => $this->unit,
        ];
    }

    public function with($request) {
      return  [
        'version' => '1.0.0',
        'author_url' => 'Gaj Cakarevic and Kevin van den Berge'
      ];
    }
}
