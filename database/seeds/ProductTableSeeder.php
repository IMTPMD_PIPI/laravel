<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();
        $json = File::get("database/data/producten.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          Product::create(array(
            'name' => $obj->name,
            'price' => $obj->price,
            'unit' => $obj->unit,
            'type' => $obj->type,
          ));
        }
    }
}
